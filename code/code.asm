:bios_boot
   HWN Z
   IFE Z, 0
      SET PC, bios_boot

:main
   JSR get_display 
   JSR get_keyboard

   JSR show_command 

   SET A, 0x0
   SET B, 0x8000
   HWI [DISPLAY]

   SET I, 3
   SET PC, input_loop

:show_command
   SET I, command 
   SET A, 0x8000
   JSR begin_loop   
   SET PC, POP

:input_loop
   SET C, 0
   HWI [KEYBOARD]

   IFE C, 127
      JSR backspace

   SET [0x8000+I], C

   IFN C, 0
      ADD I, 1

   SET [0x8000+I], 0x5f
   SET [0x8001+I], 0x0

   IFE C, 0xd
      JSR newline

   SET B, 0x8000
   SET A, 0x0
   HWI [DISPLAY]

   SET PC, input_loop


:newline
   SET [0x8000+I], 47
   ADD I, 1
   SET [0x8000+I], 62
   ADD I, 1
   SET [0x8000+I], 32
   ADD I, 1
   SET PC, POP

:backspace
   IFE I, 0
      SET PC, POP
   SET C, 0x0
   SUB I, 1
   SET PC, POP

:begin_loop
   SET [A], [I]
   ADD I, 1
   ADD A, 1
   IFN [I], 0x00
      SET PC, begin_loop
   SET PC, POP

:get_display
   ADD [DISPLAY], 1
   HWQ [DISPLAY]
   IFN A, 0xf615
      SET PC, get_display
   SET PC, POP

:get_keyboard
   ADD [KEYBOARD], 1
   HWQ [KEYBOARD]
   IFN A, 0x7406
      SET PC, get_keyboard
   SET PC, POP ; test
   
:command
   DAT "/>", 0x00

:DISPLAY 
   DAT 0xffff 

:KEYBOARD
   DAT 0xffff

:exit
