Copyright (c) 2010 Timothy Bogdala (http://www.animal-machine.com)
GPL version 3 or later (see http://www.gnu.org/licenses/gpl.html)

> import qualified Data.Maybe as DM
> import qualified Data.Map as DMap
> import qualified System.Random as R
> import qualified Control.Monad as CM
> import qualified Graphics.UI.SDL as SDL
> import qualified Graphics.UI.SDL.Image as SDLi


List all of the art files that will be used.

> artFilePaths = [ "art/64x74_blue.png",
>  	       	   "art/64x74_green.png",
>		   "art/64x74_white.png",
>		   "art/64x74_brown.png" ]


The TerrainType enumeration is used as the key for TerrainSurfaces,
for easy access to the SDL Surface. The TerrainMap has the TerrainType
as a value indexed by a 2d point.

> data TerrainType = TTh_Blue | TTh_Green | TTh_White | TTh_Brown
>      deriving (Bounded, Eq, Enum, Ord, Show)

> type Point = (Int, Int)
> type TerrainSurfaces = [(TerrainType, SDL.Surface)]
> type TerrainMap = DMap.Map Point TerrainType


Simple definitions to get the max index for the TerrainType enum,
and a list of all enumeration values.

> terrainMaxBound :: Int
> terrainMaxBound = fromEnum (maxBound :: TerrainType)

> terrainTypes :: [TerrainType]
> terrainTypes = enumFrom TTh_Blue


Generates a variable length list of random TerrainType values.

> getRandomTerrain :: Int -> IO [TerrainType]
> getRandomTerrain l = do
>     randomNumbers <- CM.replicateM l $ R.randomRIO (0,terrainMaxBound)
>     return $ map toEnum randomNumbers


Creates a random 2d map.

> makeRandomMap :: Int -> Int -> IO (TerrainMap)
> makeRandomMap w h = do
>    CM.foldM (\m y -> makeRow w y m) DMap.empty [1..h]
>   where
>      makeRow :: Int -> Int -> TerrainMap -> IO (TerrainMap)
>      makeRow w y tileMap = do
>	   rt <- getRandomTerrain w
>	   let tp = zip [1..w] rt
>	   return $ foldr (\(x,t) m -> DMap.insert (x,y) t m)  tileMap tp


In an IO action, load all of the artwork used in the map.

> loadArt :: [String] -> IO TerrainSurfaces
> loadArt paths = do
>      tileSurfs <- mapM SDLi.load paths
>      return $ zip terrainTypes tileSurfs


This method draws the TerrainType associated Surface onto another
surface (probably the main screen).

Note that it does not update the destination surface, which will neeed
to be flipped before the effects of this function can be seen.

> drawTile :: SDL.Surface -> TerrainSurfaces -> TerrainMap -> Point -> IO ()
> drawTile mainSurf terrainSurfs tm (x,y) = do
>      let sr = Just (SDL.Rect 0 0 64 74)
>      let dr = Just $ getHexmapOffset 64 74 x y
>      let tt = DM.fromJust $ DMap.lookup (x,y) tm
>      let terrainSurf = DM.fromJust $ lookup tt terrainSurfs
>      SDL.blitSurface terrainSurf sr mainSurf dr
>      return ()


This does some trickery with numbers to get the tiles to display
in the well known hex grid format. Requires shifting the X value
by half a tile for even rows, and a linear scale of 1/4 a tile
height subtracted from what would otherwise be the y offset.

> getHexmapOffset :: Int -> Int -> Int -> Int -> SDL.Rect
> getHexmapOffset tileW tileH x y = 
>      SDL.Rect adjX adjY 0 0
>   where
>      baseAdjX = (tileW * (x-1))
>      baseAdjY = (tileH * (y-1))
>      quarterH = tileH `div` 4
>      halfW = tileW `div` 2
>      adjX = if odd y
>                then baseAdjX + halfW
>		 else baseAdjX
>      adjY = baseAdjY - ((y-1) * quarterH)
      	      	 

The main worker beast for the program. 

1. Initializes SDL
2. Creates a window and sets its caption
3. Setup the SDL Surfaces for the png file artwork
4. Generate a random map
5. Draw the map to screen and update the display.
6. Wait for a keypress, then tear everything down.
7. Free the SDL Surfaces and quit.

> main :: IO ()
> main = do 
>      SDL.init [SDL.InitEverything]
>      SDL.setVideoMode 640 480 32 []    -- no flags for hw, fullscreen or anthing...
>      SDL.setCaption "Video Test!" "video test"
>
>      mainSurf <- SDL.getVideoSurface
>      tileSurfs <- loadArt artFilePaths
>
>      randomMap <- makeRandomMap 9 8
>
>      mapM_ (drawTile mainSurf tileSurfs randomMap) $ DMap.keys randomMap
>      SDL.flip mainSurf
>
>      eventLoop
>      mapM_ freeSurf tileSurfs
>      SDL.quit
>      print "done"
>  where
>      freeSurf (_ , s) = SDL.freeSurface s
>      eventLoop = SDL.waitEventBlocking >>= checkEvent
>      checkEvent (SDL.KeyUp _) = return ()
>      checkEvent _	    = eventLoop

		
