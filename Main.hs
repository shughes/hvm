module Main where

import Dcpu
import Screen 
import Common
import Assembler
import Data.Char
import Data.Bits hiding (rotate)
import Control.Monad.State hiding (get)
import Data.Word
import Graphics.UI.GLUT hiding (rotate)
import Control.Monad.State
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.STM.TChan
import System.IO

inCallback regVar regs mem = do
   cReg <- wait
   let regs2 = putRegVals regs [(2, cReg)]
   return (regs2, mem)
  where 
     wait = do
        cReg <- atomically $ do
                  let c = getRegVal regs 2
                  cReg <- readTVar regVar
                  writeTVar regVar 0
                  if cReg == 0
                     then return c
                     else return cReg
        if cReg == 0 
           then wait
           else do 
               return cReg
   

outCallback regsVar memVar regs mem = do
   atomically $ do 
      writeTVar regsVar regs
      writeTVar memVar mem
   postRedisplay Nothing
   return (regs, mem)

runVm code devs = do
   load code
   mapM_ registerDevice devs
   run

main' = do
   file <- readFile "code/firefox.hex"
   let arr1 = split file ',' 
       arr2 = (map (read . trim) arr1)::[Word16]
   setupWindow (showImage arr2) Nothing

main = do
   code <- compile "code/code.asm"
   memVar <- newTVarIO initMem
   regVar <- newTVarIO 0
   regsVar <- newTVarIO initRegs
   
   let devs = [initDevice 0x7349f615 0x1c6c8b36 0x1802 "LEM1802" (outCallback regsVar memVar),
               initDevice 0x30cf7406 0x0 0x1 "Generic Keyboard" (inCallback regVar)]

   forkIO $ do 
      execStateT (runVm code devs) initVm
      return () 

   setupWindow (displayPixels regsVar memVar) (Just (inputLoop regVar))


