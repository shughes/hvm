module Assembler 
   ( compile
   , parseInstructions ) where

import Common
import Data.Word
import Data.Char
import Data.Bits
import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.ByteString as B
import Control.Monad.Identity
import Control.Monad.State
import Text.ParserCombinators.Parsec hiding (State)
import Text.Parsec.Prim hiding (State)

data Row = Instruction Opcode Value Value
         | Function String
         | SpecialInstr Opcode Value
         | Comment String
         | Data [Word16]
         | EndOfFile
           deriving Show

data Value = AddressLit Word16
           | AddressReg Register
           | Literal Word16
           | Reg Register
           | FunctionRef String
           | AddressFunctionRef String
           | AddressOfRegAndLit Register Word16
           | AddressOfFuncAndLit String Word16
           | AddressOfFuncAndReg String Register
           | NoValue
             deriving Show

data Register = Z | X | Y | A | B | C | I | J | PUSH | POP | PC | SP | EX
                deriving (Show, Read)

data Opcode = ADD | AND | SET | SUB | MUL | MLI | DIV | IFE | IFN | 
              JSR | BOR | XOR | SHL | SHR | DAT | HWI | HWQ | HWN | 
              IFG | MOD | IFB | IAS | IAG
              deriving (Show, Read, Eq)

type Instructions = StateT (M.Map String (Word16, [Word16]), [Word16]) IO


class ToWord a where
   toWord :: a -> Word16

instance ToWord Opcode where
   toWord SET = 0x1
   toWord JSR = 0x1
   toWord ADD = 0x2
   toWord SUB = 0x3
   toWord MUL = 0x4
   toWord MLI = 0x5
   toWord MOD = 0x8
   toWord DIV = 0x6
   toWord IAG = 0x9
   toWord IAS = 0xa
   toWord AND = 0xa
   toWord BOR = 0xb
   toWord XOR = 0xc
   toWord HWN = 0x10
   toWord IFB = 0x10
   toWord HWQ = 0x11
   toWord IFE = 0x12
   toWord HWI = 0x12
   toWord IFN = 0x13
   toWord IFG = 0x14
   

instance ToWord Register where
   toWord A = 0x0
   toWord B = 0x1
   toWord C = 0x2
   toWord X = 0x3
   toWord Y = 0x4
   toWord Z = 0x5
   toWord I = 0x6
   toWord J = 0x7
   toWord PUSH = 0x18
   toWord POP = 0x18
   toWord PC = 0x1c
   toWord SP = 0x1b
   toWord EX = 0x1d


-- compiler

compile file = do
   (m, arr) <- execStateT (assemble file) (M.empty, [])
   return arr

assemble :: String -> Instructions ()
assemble file = do
   Right rows <- lift $ parseInstructions file
   mapM_ assembleRow rows
   (funcs, _) <- get
   put (funcs, [])
   mapM_ assembleRow rows
   return ()

assembleRow :: Row -> Instructions ()
assembleRow (Data arr) = do
   (funcs, ram) <- get
   put (funcs, ram ++ arr)
assembleRow (SpecialInstr op val1) = do
   let op' = toWord op
   (val:rest) <- handleVal val1
   let row = (op' `shiftL` 5) + (val `shiftL` 10)
   (funcs, ram) <- get
   put (funcs, ram ++ [row] ++ rest)
assembleRow (Instruction op val1 val2) = do
   let op' = toWord op
   (val1':rest1) <- handleVal val1
   (val2':rest2) <- handleVal val2
   let row1 = op' + (val1' `shiftL` 5) + (val2' `shiftL` 10)
       fullRow = [row1] ++ rest1 ++ rest2
   (funcs, ram) <- get
   put (funcs, ram ++ fullRow)
assembleRow (Function func) = do
   (funcs, ram) <- get
   let size = (fromIntegral (L.length ram)) :: Word16
       mfunc = M.lookup func funcs
       funcs' = maybe (M.insert func (size, []) funcs) 
         (\(_, refs) -> M.insert func (size, refs) funcs) mfunc
   put (funcs', ram)
assembleRow other = return ()

handleVal (Reg reg) = return [toWord reg]
handleVal (Literal lit) = return [0x1f, lit]
handleVal (AddressOfFuncAndReg ref reg) = do
   (_:[addr]) <- handleVal (FunctionRef ref)
   return [(toWord reg)+0x10, addr]
handleVal (AddressOfFuncAndLit ref lit) = do
   (_:[addr]) <- handleVal (FunctionRef ref)
   return [0x1e, addr+lit]
handleVal (AddressOfRegAndLit reg lit) = return [(toWord reg)+0x10, lit]
handleVal (AddressLit lit) = return [0x1e, lit]
handleVal (AddressReg reg) = return [(toWord reg)+0x8]
handleVal (AddressFunctionRef ref) = do
   (_:addr) <- handleVal (FunctionRef ref)
   return (0x1e:addr)
handleVal (FunctionRef ref) = do
   (funcs, ram) <- get
   let mfunc = M.lookup ref funcs
       addr = maybe 0 fst mfunc
   return [0x1f, addr]



-- Parser

asmFile = instructionVal 

instructionVal :: ParsecT [Char] st Identity [Row]
instructionVal = do
   fullRow `sepBy` newline 

fullRow = do
   skipMany newline
   spaces
   comment <|> function <|> end <|> row  

end = do
   eof
   return EndOfFile
   
comment = do
   char ';'
   val <- many $ noneOf "\n"
   return $ Comment val

function = do
   char ':'
   val <- many $ noneOf ";\n"
   removeComment
   return $ Function (trim val)

row :: ParsecT [Char] st Identity Row
row = do
   spaces
   opStr <- opcode
   spaces
   let op = (read opStr)::Opcode
       instr = map (==op) [SET, ADD, MUL, MLI, MOD, AND, 
                           SUB, DIV, XOR, BOR, IFB, IFE, 
                           IFN, IFG]
   if (or instr)
      then do
         val1 <- parseValue
         spaces
         char ','
         spaces
         val2 <- parseValue
         return $ Instruction op val1 val2
      else do
         if (op==DAT) 
            then parseData
            else do
               val1 <- parseValue
               return $ SpecialInstr op val1

parseValue :: ParsecT [Char] st Identity Value
parseValue = address <|> literal <|> variable 

parseData = do 
   str <- many $ noneOf ";\n"
   removeComment
   let arr = split str ','
       arr2 = map trim arr
       arr3 = L.concat $ map handleRecord arr2
   return $ Data arr3
   where
      handleRecord record = 
         case record of
              ('"':rest) -> let arr = L.reverse (tail $ L.reverse rest)
                                arr2 = map (fromIntegral . ord) arr
                            in arr2
              otherwise -> [fromIntegral $ read record]


opcode = do
   op <- many $ letter
   let opStr = map toUpper op
   return opStr

sumIndex = do
   spaces
   char '+'
   spaces
   parseValue

variable = do
   c <- letter
   rest <- valueString
   let val = map toUpper (c:rest)
       arr = map (==val) ["SP", "POP", "PUSH", "PC", "EX",
                          "A", "B", "C", "X", "Y", "Z", "I", "J"]
   if (or arr) 
      then return $ Reg ((read val)::Register)
      else return $ FunctionRef (c:rest)

address :: ParsecT [Char] st Identity Value
address = do
   char '['
   val1 <- parseValue
   val2 <- option NoValue sumIndex
   char ']'
   address' val1 val2
   where
      address' (Reg reg) (FunctionRef ref) = return $ AddressOfFuncAndReg ref reg
      address' (FunctionRef ref) (Reg reg) = return $ AddressOfFuncAndReg ref reg
      address' (FunctionRef ref) (Literal lit) = return $ AddressOfFuncAndLit ref lit
      address' (Literal lit) (FunctionRef ref) = return $ AddressOfFuncAndLit ref lit
      address' (Literal lit) (Reg reg) = return $ AddressOfRegAndLit reg lit
      address' (Reg reg) (Literal lit) = return $ AddressOfRegAndLit reg lit
      address' (Reg reg) _ = return $ AddressReg reg
      address' (FunctionRef ref) _ = return $ AddressFunctionRef ref
      address' (Literal lit) _ = return $ AddressLit lit

removeComment = do
   option "" (do char ';'
                 many $ noneOf "\n")

valueString = do
   val <- many $ noneOf ";+],\n"
   removeComment
   return $ trim val

literal :: ParsecT [Char] st Identity Value
literal = do
   c <- digit
   val <- valueString
   return $ Literal (fromIntegral $ read (c:val))

parseInstructions fileName = do
   file <- readFile fileName 
   return $ parse asmFile "(unknown)" file

