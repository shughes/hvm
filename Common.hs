module Common where
   
import Data.Word
import qualified Data.List as L
import qualified Data.Map as M
import Data.Vector (Vector)
import qualified Data.Vector as Vector

type Regs = Vector Word16

type Mem = M.Map Word16 Word16

initRegs :: Regs
initRegs = Vector.replicate 8 0

initMem :: Mem
initMem = M.empty

getRegVal :: Vector Word16 -> Word16 -> Word16
getRegVal regs i = regs Vector.! (fromIntegral i)

putRegVals regs vals = regs Vector.// vals

insertArr i arr1 arr2 = 
   let n = fromIntegral (L.length arr2 - 1) :: Word16
       ai = [i..(i+n)]
   in foldl (\ m (key, val) -> M.insert key val m) arr1 (zip ai arr2)

getArr arr i n = 
   map (\i -> let mval = M.lookup i arr
              in maybe 0 id mval) [i..(i+n-1)]

trim str = 
   let arr1 = trim' str
       arr2 = trim' $ L.reverse arr1
   in L.reverse arr2
   where
      trim' [] = []
      trim' (' ':str) = trim' str
      trim' ('\n':str) = trim' str
      trim' str = str

split str cmp = 
   let val1 = L.takeWhile (/= cmp) str
       val2 = L.dropWhile (/= cmp) str
       res = case val2 of
                  [] -> []
                  otherwise -> split (tail val2) cmp
       ret = case val1 of
                  [] -> res
                  otherwise -> (val1:res)
   in ret
