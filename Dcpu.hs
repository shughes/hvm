module Dcpu 
   ( load
   , run
   , step
   , registerDevice
   , initDevice
   , initVm
   , Vm ) where

import Assembler
import Common
import Data.Word
import Data.Int
import Data.Bits
import Data.Char
import Control.Monad.State
import qualified Data.List as L
import qualified Data.Map as M
import Data.IORef
import Data.Array.IArray
import Data.Functor.Identity
import System.IO


data Vm = Vm { vmRegs :: Regs
             , vmMem :: Mem
             , vmSp :: Word16 
             , vmPc :: Word16
             , vmOv :: Word16
             , vmIa :: Word16
             , vmDevices :: [Device]
             , vmNextPc :: Word16
             , vmLastPc :: Word16
             , vmSkip :: Bool }

data Device = Device { devHwId :: Int
                     , devManId :: Int
                     , devVer :: Word16 
                     , devLabel :: String
                     , devCallback :: (Regs -> Mem -> IO (Regs, Mem)) }

instance Show Vm where
   show vm = let regs = vmRegs vm
                 regVals = "A: " ++ (show $ getRegVal regs 0) ++ ", " ++
                           "B: " ++ (show $ getRegVal regs 1) ++ ", " ++ 
                           "C: " ++ (show $ getRegVal regs 2) ++ ", " ++
                           "X: " ++ (show $ getRegVal regs 3) ++ ", " ++
                           "Y: " ++ (show $ getRegVal regs 4) ++ ", " ++
                           "Z: " ++ (show $ getRegVal regs 5) ++ ", " ++
                           "I: " ++ (show $ getRegVal regs 6) ++ ", " ++
                           "J: " ++ (show $ getRegVal regs 7) 
                 sp = show $ vmSp vm
                 pc = show $ vmPc vm
                 ov = show $ vmOv vm
                 mem = show $ vmMem vm
             in "\nSP: " ++ sp ++ ", PC: " ++ pc ++ ", EX: " ++ ov ++
                 "\n Mem: " ++ mem


data Result = Reg { ptr :: Word16, val :: Word16 }
            | Mem { ptr :: Word16, val :: Word16 }
            | Val { val :: Word16 }
            | Pc { val :: Word16 }
            | Ov { val :: Word16 }
            | Sp { val :: Word16 }
              deriving Show

type VmState = StateT Vm IO

initVm = Vm { vmRegs = initRegs
           , vmMem = initMem
           , vmSp = 65534
           , vmPc = 0
           , vmOv = 0 
           , vmIa = 0
           , vmDevices = []
           , vmNextPc = 0
           , vmLastPc = 0
           , vmSkip = False }


load instructions = do
   putMem 0 instructions
   putLastPc (fromIntegral $ L.length instructions)

run = do
   step
   exit <- getLastPc
   next <- getNextPc
   case next >= exit of
        True -> return ()
        False -> run

step = do
   pc <- getPc
   (instr:_) <- getMem pc 1
   let opcode' = instr .&. 0x1f
       a1 = (shiftR instr 5) .&. 0x1f
       b1 = shiftR instr 10
   (opcode, a2, b2) <- 
      case opcode' == 0x0 of
           True -> do
              a2 <- handleVal True b1
              return (a1, a2, a2)
           False -> do
              a2 <- handleVal True a1
              b2 <- handleVal False b1
              return (opcode', a2, b2)
   skip <- getSkip
   pc <- getPc
   putNextPc (pc+1)
   case skip of 
        True -> do putSkip False
        False -> case opcode' == 0x0 of
                      True -> do 
                           handleSpecialOp opcode a2
                      False -> do
                           handleOpcode opcode a2 b2
   pc <- getNextPc
   putPc pc


putVal (Ov ov) (val:_) = putOv val
putVal (Sp sp) (val:_) = putSp val
putVal (Pc pc) (val:_) = do
   putNextPc val
putVal (Mem p _) val = do
   putMem p val
putVal (Reg p _) (val:_) = putReg p val

handleSpecialOp opcode a = do
   case opcode of
        -- JSR
        0x1 -> do 
            push
            sp <- getSp
            pc <- getPc
            putMem sp [pc+1]
            putVal (Pc pc) [val a]

        -- IAG g
        0x09 -> do
            val <- getIa
            putVal a [val]

        -- IAS a
        0x0a -> putIa (val a) 

        -- HWN a
        0x10 -> do
            devs <- getDevices
            let c = (fromIntegral $ L.length devs)
            putVal a [c]

        -- HWQ a
        0x11 -> do
            devs <- getDevices
            let i = val a
                dev = devs !! (fromIntegral i)
                hwId = devHwId dev
                b1 = (fromIntegral $ (hwId .&. 0xffff0000) `shiftR` 0x10)
                a1 = (fromIntegral $ hwId .&. 0x0000ffff)
                manId = devManId dev
                y1 = (fromIntegral $ (manId .&. 0xffff0000) `shiftR` 0x10)
                x1 = (fromIntegral $ manId .&. 0x0000ffff)
                ver = devVer dev
            putReg 0 a1
            putReg 1 b1
            putReg 2 ver
            putReg 3 x1
            putReg 4 y1

        -- HWI a
        0x12 -> do
            devs <- getDevices
            let i = (fromIntegral $ val a)
            callDevice (devs !! i) 

        otherwise -> return ()

handleOpcode opcode a b = do
   let a2 = fromIntegral (val a) 
       b2 = fromIntegral (val b) 
   case opcode of
        -- SET
        0x1 -> putVal a [val b]

        -- ADD
        0x2 -> do 
            let val = a2 + b2
                ov = case val >= 0x10000 of
                          True -> 0x1
                          False -> 0x0
                val16 = fromIntegral val 
            putVal a [val16]
            putOv ov

        -- SUB
        0x3 -> do 
            let val = a2 - b2
                ov = case val < 0x0 of
                          True -> 0xffff
                          False -> 0x0
                val16 = fromIntegral val 
            putVal a [val16]
            putOv ov

        -- MUL
        0x4 -> do let val = a2 * b2
                      ov = fromIntegral ((val `shiftR` 16) .&. 0xffff) 
                      val16 = fromIntegral val 
                  putVal a [val16]
                  putOv ov

        -- DIV
        0x6 -> case b2 of
                    0 -> do putVal a [0]
                            putOv 0
                    otherwise -> do
                        let val = a2 `div` b2
                            val16 = fromIntegral val 
                            ov = fromIntegral (((a2 `shiftL` 16) `div` b2) .&. 0xffff) 
                        putVal a [val16]
                        putOv ov

        -- MOD
        0x8 -> do let val = if b2 == 0 then 0 else mod a2 b2 
                      val16 = fromIntegral val 
                  putVal a [val16]

        -- AND
        0xa -> do let val = a2 .&. b2
                      val16 = fromIntegral val 
                  putVal a [val16]

        -- BOR
        0xb -> do let val = a2 .|. b2
                      val16 = fromIntegral val 
                  putVal a [val16]

        -- XOR
        0xc -> do let val = a2 `xor` b2
                      val16 = fromIntegral val 
                  putVal a [val16]

        -- SHL
        0xf -> do let val = shiftL a2 b2 
                      val16 = fromIntegral val 
                      ov = fromIntegral ((val `shiftR` 16) .&. 0xffff)
                  putVal a [val16]
                  putOv ov

        -- SHR
        0xd -> do let val = shiftR a2 b2 
                      val16 = fromIntegral val 
                      ov = fromIntegral (((a2 `shiftL` 16) `shiftR` b2) .&. 0xffff)
                  putVal a [val16]
                  putOv ov

        -- IFB
        0x10 -> if (a2 .&. b2) == 0 
                  then putSkip True
                  else return ()

        -- IFC
        0x11 -> if (a2 .&. b2) == 0
                  then return ()
                  else putSkip True

        -- IFE
        0x12 -> if a2 == b2 
                  then return ()
                  else putSkip True

        -- IFN
        0x13 -> if a2 == b2 
                  then putSkip True
                  else return ()

        -- IFG
        0x14 -> if a2 > b2
                  then return ()
                  else putSkip True

        



handleVal isA val 
     -- register
   | val < 0x08 = do
         result <- getReg val
         return $ Reg val result

     -- [register]
   | and [val >= 0x08, val < 0x10] = do
         r <- getReg (val .&. 0x7)
         (m:_) <- getMem r 1
         return $ Mem r m

     -- [register + next word]
   | and [val >= 0x10, val < 0x17] = do
         pc <- getPc 
         putPc (pc+1)
         (nextWord:_) <- getMem (pc+1) 1
         r <- getReg (val .&. 0xf)
         (m:_) <- getMem (nextWord + r) 1
         return $ Mem (nextWord + r) m

     -- PUSH / POP
   | val == 0x18 = case isA of
                        True -> do push
                                   sp <- getSp
                                   m <- peek
                                   return $ Mem sp m
                        False -> do sp <- getSp
                                    val <- pop
                                    return $ Mem sp val

     -- [SP] / PEEK
   | val == 0x19 = do val <- peek
                      sp <- getSp
                      return $ Mem sp val

     -- [SP + next word] / PICK n
   | val == 0x1a = do
         sp <- getSp
         pc <- getPc
         (val:_) <- getMem (pc+1) 1
         (val2:_) <- getMem (val+sp) 1
         putPc (pc+1)
         return $ Mem (val+sp) val2

     -- SP
   | val == 0x1b = do 
         sp <- getSp
         return $ Sp sp

     -- PC
   | val == 0x1c = do 
         pc <- getPc
         return $ Pc pc

     -- EX
   | val == 0x1d = do 
         ov <- getOv
         return $ Ov ov

     -- [next word]
   | val == 0x1e = do
         pc <- getPc
         putPc (pc+1)
         (m:_) <- getMem (pc+1) 1
         (m2:_) <- getMem m 1
         return $ Mem m m2

     -- next word (literal)
   | val == 0x1f = do
         pc <- getPc
         putPc (pc+1)
         (m:_) <- getMem (pc+1) 1
         return $ Mem (pc+1) m
     
     -- literal value
   | and [val > 0x1f, val <= 0x3f] = do 
         let literal = [0x80..0x1f] !! (fromIntegral (val .&. 0x1f))
         return $ Val literal


push = do
   skip <- getSkip
   sp <- getSp
   case skip of
        True -> return ()
        False -> putSp (sp-1)

pop = do
   v <- peek
   sp <- getSp
   skip <- getSkip
   case skip of
        True -> return ()
        False -> putSp (sp+1)
   return v

peek = do
   sp <- getSp
   arr <- getMem sp 1
   case (L.length arr) > 0 of
        True -> return $ L.head arr
        False -> return 0

initDevice hw manu ver label call = Device hw manu ver label call

registerDevice dev = do
   vm <- get 
   devs <- getDevices
   put $ vm { vmDevices = devs ++ [dev] }

callDevice dev = do
   vm <- get
   let f = devCallback dev
       regs = vmRegs vm
       mem = vmMem vm
   (r2, m2) <- lift $ f regs mem
   put $ vm { vmRegs = r2, vmMem = m2 }


-- Getters/Setters

getDevices = do
   vm <- get
   return $ vmDevices vm

putLastPc pc = modify $ \vm -> vm { vmLastPc = pc }

getLastPc :: VmState Word16
getLastPc = do 
   vm <- get
   return $ vmLastPc vm

putSkip val = modify $ \vm -> vm { vmSkip = val }

getSkip :: VmState Bool
getSkip = do vm <- get
             return $ vmSkip vm

putSp val = modify $ \vm -> vm { vmSp = val }

getSp :: VmState Word16
getSp = do vm <- get
           return $ vmSp vm

putPc val = modify $ \vm -> vm { vmPc = val }

getPc :: VmState Word16
getPc = do vm <- get
           return $ vmPc vm

putNextPc val = modify $ \vm -> vm { vmNextPc = val }

getNextPc :: VmState Word16
getNextPc = do vm <- get
               return $ vmNextPc vm

putOv val = modify $ \vm -> vm { vmOv = val }

getOv :: VmState Word16
getOv = do vm <- get
           return $ vmOv vm

putIa ia = do
   vm <- get
   put $ vm { vmIa = ia }

getIa = do
   vm <- get
   return $ vmIa vm

putMem :: Word16 -> [Word16] -> VmState ()
putMem i arr = do
   vm <- get
   let mem = vmMem vm
       mem2 = insertArr i mem arr
   put $ vm { vmMem = mem2 }


getMem :: Word16 -> Word16 -> VmState [Word16]
getMem i n = do
   vm <- get
   let mem = vmMem vm
   return $ getArr mem i n


putReg :: Word16 -> Word16 -> VmState ()
putReg r val = do
   vm <- get
   let regs = vmRegs vm
       regs2 = putRegVals regs [(fromIntegral r, val)] 
   put $ vm { vmRegs = regs2 } 

getReg :: Word16 -> VmState Word16
getReg r = do
   vm <- get
   let regs = vmRegs vm
   return $ getRegVal regs r


