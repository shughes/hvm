module Screen 
   ( displayPixels
   , setupWindow
   , showImage
   , inputLoop ) where

import qualified Common as C
import Dcpu
import Ascii 
import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT.Callbacks.Window
import Graphics.UI.GLUT hiding (rotate)
import Data.Word
import Data.Char
import Data.IORef
import Data.Bits hiding (rotate)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Marshal.Alloc (free)
import Foreign.Marshal.Array
import System.IO
import qualified Data.List as L
import Control.Concurrent
import Control.Concurrent.STM


twoVals [] = []
twoVals (a:b:rest) = ((a, b):twoVals rest)

reshape s@(Size w h) = do
   viewport $= (Position 0 0, s)
   postRedisplay Nothing

-- GUI

data Pixel = Pixel Word16 Word16 (Color3 GLfloat) deriving Show

colSize :: Word16
colSize = 42

charWidth = 4

charHeight = 8

toCharXY i = let (y, x) = divMod i colSize
             in (x*charWidth, y*charHeight)

toColors :: Word8 -> (Color3 GLfloat, Color3 GLfloat)
toColors word = 
   let a = (word `shiftR` 4) .&. 0xf
       b = word .&. 0xf
   in (toColor a, toColor b)

toColor i
   | i == 0x0 = Color3 (0.0::GLfloat) 0 0 -- black 
   | i == 0x1 = Color3 (0.11::GLfloat) 0.15 0.20 -- navy
   | i == 0x2 = Color3 (0.27::GLfloat) 0.54 0.10 -- green
   | i == 0x3 = Color3 (0.19::GLfloat) 0.28 0.31 -- dark aqua
   | i == 0x4 = Color3 (0.89::GLfloat) 0.44 0.55 -- pink
   | i == 0x5 = Color3 (0.6::GLfloat) 0 0 -- red 
   | i == 0x6 = Color3 (0.65::GLfloat) 0.4 0.13 -- light brown
   | i == 0x7 = Color3 (0.75::GLfloat) 0.75 0.75 -- light gray
   | i == 0x8 = Color3 (0.4::GLfloat) 0.4 0.4 -- gray
   | i == 0x9 = Color3 (0.0::GLfloat) 0.0 1.0 -- blue
   | i == 0xa = Color3 (0.92::GLfloat) 0.54 0.19 -- orange
   | i == 0xb = Color3 (0.64::GLfloat) 0.81 0.15 -- light green
   | i == 0xc = Color3 (0.19::GLfloat) 0.64 0.95 -- light aqua
   | i == 0xd = Color3 (0::GLfloat) 0.34 0.52 -- aqua
   | i == 0xe = Color3 (0.98::GLfloat) 0.89 0.42 -- yellow 
   | i == 0xf = Color3 (0.67::GLfloat) 0.86 0.94 -- sky blue
   | otherwise = Color3 (1.0::GLfloat) 1 1

defineColors word n a b =
   let bits = toBits word n
       result = map defineColors' bits
   in result
   where 
      defineColors' 1 = a
      defineColors' 0 = b

      toBits word 0 = [(shiftR word 0) .&. 1]
      toBits word shift = ((shiftR word shift) .&. 1 : toBits word (shift-1))


setupWindow display input= do
   (progname, _) <- getArgsAndInitialize
   createWindow "Window"
   windowSize $= Size 640 480 --128 96
   clear [ColorBuffer]

   matrixMode $= Projection
   loadIdentity
   ortho 0 640 480.0 0 0 1.0

   matrixMode $= Modelview 0
   loadIdentity
   scale 3.0 3.0 (3.0::GLfloat)
   displayCallback $= display
   keyboardMouseCallback $= input
   mainLoop

toCharVal i word1 word2 color1 color2 = 
   let (x, y) = toCharXY i
       arr1 = (defineColors word1 15 color1 color2) ++ 
              (defineColors word2 15 color1 color2)
       r1 = take 8 arr1
       arr2 = drop 8 arr1
       r2 = take 8 arr2
       arr3 = drop 8 arr2
       r3 = take 8 arr3
       arr4 = drop 8 arr3
       r4 = take 8 arr4
       result = (toVals r1 (x+0) y) ++ (toVals r2 (x+1) y) ++ 
                (toVals r3 (x+2) y) ++ (toVals r4 (x+3) y)
   in result
  where
     toVals [] x y = []
     toVals (b:rest) x y = 
        (Pixel x (y+(fromIntegral $ L.length rest)+1) b : toVals rest x y)

toChar :: Word16 -> Word16 -> [Pixel]
toChar i letter = 
   let [word1, word2] = fromAscii letter1
       letter1 = letter .&. 0xff
       colors = fromIntegral (shiftR (letter .&. 0xff00) 8)
       (color1, color2) = if colors == 0
            then (Color3 1 1 1, Color3 0 0 0)
            else toColors colors
   in toCharVal i word1 word2 color1 color2


toCell :: Word16 -> Word16 -> [Pixel]
toCell i word = 
   let (a, b) = toColors (fromIntegral (word `shiftR` 8)::Word8)
       bits = fromIntegral (word .&. 0xff)::Word8
       (x, y) = toXY i
       arr = (defineColors bits 7 a b) ++ [b]
       r1 = take 3 arr
       arr2 = drop 3 arr
       r2 = take 3 arr2
       arr3 = drop 3 arr2
       r3 = take 3 arr3
   in [Pixel x (y+0) (r1 !! 0), Pixel (x+1) (y+0) (r1 !! 1), Pixel (x+2) (y+0) (r1 !! 2),
       Pixel x (y+1) (r2 !! 0), Pixel (x+1) (y+1) (r2 !! 1), Pixel (x+2) (y+1) (r2 !! 2),
       Pixel x (y+2) (r3 !! 0), Pixel (x+1) (y+2) (r3 !! 1), Pixel (x+2) (y+2) (r3 !! 2)]
  where
     toXY i = let (y, x) = divMod i colSize
              in (x*3, y*3)


pixel (Pixel x1 y1 c) = do
   color c
   let w = 1
       x = (fromIntegral x1)::GLfloat
       y = (fromIntegral y1)::GLfloat
   renderPrimitive Quads $ do
      vertex $ Vertex2 x (y::GLfloat)
      vertex $ Vertex2 x (y+w)
      vertex $ Vertex2 (x+w) (y+w)
      vertex $ Vertex2 (x+w) y


-- if newline char
nextIndex i 0x0a = nextIndex i 0x0d
nextIndex i 0x0d =
   let (x,y) = toCharXY i
       w = colSize * charWidth - charWidth
       buffer = (fromIntegral $ (w - x) `div` charWidth)::Int
   in 1+i+((fromIntegral buffer)::Word16)
--
-- else
nextIndex i val = i+1

showImage arr = do
   clear [ColorBuffer]
   drawChar 0 arr
   flush
  where
   drawChar i [] = return ()
   drawChar i (a:arr) = do
      mapM_ pixel (toCell i a)
      drawChar (i+1) arr

getImageArray file = do
   str <- readFile file
   let arr1 = C.split str ',' 
       arr2 = (map (read . C.trim) arr1)::[Word16]
   return arr2
   
displayPixels devRegsVar devMemVar = do
   regs <- readTVarIO devRegsVar
   mem <- readTVarIO devMemVar
   let b = C.getRegVal regs 1
       c = 0x155 
       arr = C.getArr mem b c
   clear [ColorBuffer]
   drawChar 0 arr 
   flush
  where
     drawChar i [] = return ()
     drawChar i (a:arr) = do
        let nextI = nextIndex i a
            isZero = (a .&. 0xff) == 0
            pixelArr = if isZero
                          then []
                          else toChar i a 
        case isZero of
             True -> drawChar nextI arr
             False -> do mapM_ pixel pixelArr
                         drawChar nextI arr
     
     drawCell i [] = return ()
     drawCell i (a:arr) = do
        mapM_ pixel (toCell i a)
        drawChar (i+1) arr

inputLoop regVar (Char key) Down mods pos = do
   atomically $ do
      let val = fromIntegral $ ord key
      writeTVar regVar val
   postRedisplay Nothing

inputLoop regVar key keystate mods pos = return ()
