import Control.Monad
import Text.ParserCombinators.Parsec

data StateT st m a = StateT { runStateT :: (st -> m (a, st)) }

instance Monad m => Monad (StateT st m) where
   ma >>= f = StateT $ \st ->
      let mresult = runStateT ma st
      in do (a, st) <- mresult
            runStateT (f a) st
   return a = StateT $ \st -> return (a, st)
