module Vm where

import Data.Word
import Data.Bits
import Control.Monad.State
import Codec.Binary.UTF8.String
import qualified Data.List as L
import qualified Data.Map as M
import Data.Array.Diff
import Data.Functor.Identity


data Vm = Vm { vmRegs :: DiffArray Word16 Word16
             , vmMem :: DiffArray Word16 Word16  
             , vmSp :: Word16 
             , vmPc :: Word16
             , vmOv :: Word16 }
               deriving Show


initVm = Vm { vmRegs = (listArray (0,8) [ 0 | x <- [0..7]]) 
            , vmMem = (listArray (0,65535) [ 0 | x <- [0..65535]])
            , vmSp = 65535
            , vmPc = 0
            , vmOv = 0 }


putReg r val = modify (\vm -> vm { vmRegs = (vmRegs vm) // [(r, val)] })

getReg :: Word16 -> State Vm Word16
getReg r = gets (\vm -> vmRegs vm ! r)

putSp val = modify (\vm -> vm { vmSp = val })

getSp = gets vmSp 

putPc val = modify (\vm -> vm { vmPc = val })

getPc :: State Vm Word16
getPc = gets vmPc

putMem :: Word16 -> [Word16] -> State Vm ()
putMem i arr = modify (\vm ->
   let n = fromIntegral (L.length arr - 1) :: Word16
       ai = [i..(i+n)]
   in vm { vmMem = (vmMem vm) // zip ai arr })

getMem i n = gets (\vm -> map (\x-> (vmMem vm) ! x) [i..(i+n-1)])

getOv = gets vmOv

putOv val = modify (\vm -> vm { vmOv = val })

push :: Word16 -> State Vm ()
push val = do
   sp <- getSp
   putSp $ sp-1
   putMem (sp-1) [val]

pop :: State Vm Word16 
pop = do
   sp <- getSp
   arr <- getMem sp 1
   putSp (sp + 1)
   return $ head arr

peek = do
   sp <- getSp
   (m:_) <- getMem sp 1
   return m

{-run = do
   pc <- getPc
   (instr:_) <- getMem pc 1
   putPc (pc + 1)
   let opcode = instr .&. 0xf
       a1 = (shiftR instr 4) .&. 0x3f
       b1 = shiftR instr 10
       isReg = a1 < 0x08
   a2 <- handleVal a1
   b2 <- handleVal b1
   ov <- getOv
   let res = handleOpcode opcode a2 b2
       mOv = find (== opcode) [0x2, 0x3, 0x4, 0x5, 0x7, 0x8]
       newOv = maybe ov (\o -> shiftR res 16) mOv
       res16 = (fromIntegral (res .&. 0xFFFF))::Word16
   case and [opcode < 0xc, a1 < 0x1f] of
        True -> case isReg of
                     True -> putReg a1 res16
                     False -> case a1 of
                                   0x1b -> putSp res16
                                   0x1c -> putPc res16
                                   0x1d -> putOv res16
                                   otherwise -> putMem res16
        False -> 
   setOv newOv
   return newOv -}


handleOpcode :: Word16 -> Word16 -> Word16 -> Word32
handleOpcode opcode a2' b2' = 
   let a2 = fromIntegral a2' :: Int
       b2 = fromIntegral b2' :: Int
       res = case opcode of
                  0x1 -> b2
                  0x2 -> a2 + b2
                  0x3 -> a2 - b2
                  0x4 -> a2 * b2
                  0x5 -> if b2 == 0 then 0 else a2
                  0x6 -> if b2 == 0 then 0 else mod a2 b2 
                  0x7 -> shiftL a2 b2 
                  0x8 -> shiftR a2 b2
                  0x9 -> a2 .&. b2
                  0xa -> a2 .|. b2
                  0xb -> a2 `xor` b2
                  0xc -> if a2 == b2 then 1 else 0
                  0xd -> if a2 == b2 then 0 else 1
                  0xe -> if a2 > b2 then 1 else 0
                  0xf -> if (a2 .&. b2) == 0 then 0 else 1
   in fromIntegral res 

-- set a 35

getLiteral :: [Word16]
getLiteral = [0x00..0x1f]

handleVal val 
     -- get val in specified reg
   | val < 0x08 = getReg val
     -- get val at memory address in specified reg
   | and [val >= 0x08, val < 0x10] = do
         r <- getReg val
         (m:_) <- getMem r 1
         return m
   | and [val >= 0x10, val < 0x17] = do
         pc <- getPc 
         (nextWord:_) <- getMem pc 1
         r <- getReg val
         (m:_) <- getMem (nextWord + r) 1
         return m
   | val == 0x18 = pop
   | val == 0x19 = peek
   | val == 0x1a = do
         sp <- getSp
         putSp (sp-1)
         (m:_) <- getMem (sp-1) 1
         return m
   | val == 0x1b = getSp
   | val == 0x1c = getPc
   | val == 0x1d = getOv
   | val == 0x1e = do
         pc <- getPc
         putPc (pc+1)
         (m:_) <- getMem pc 1
         (m2:_) <- getMem m 1
         return m2
   | val == 0x1f = do
         pc <- getPc
         putPc (pc+1)
         (m:_) <- getMem pc 1
         return m
   | otherwise = return $ getLiteral !! ((fromIntegral (val .&. 0x1f))::Int)

test = do
   putMem 0 [0x7c01, 0x0030] -- set a 0x30
   putMem 2 [] -- add 

main = evalState test initVm
